const { configs } = require('eslint-plugin-prettier');
const express = require('express');
//servidor
const app = express();
//carga config del puerto
const {config} = require('./config/index');

const {
    logErrors,
    wrapErrors,
    errorHandler
  } = require('./utils/middleware/errorHandlers.js');

const notFoundHandler = require('./utils/middleware/notFoundHandler')
//importa aplicacion
const moviesApi = require('./routes/movies.js')

app.use(express.json());

//se intancia funcion de movies con express para que consulte la api
moviesApi(app);


app.use(notFoundHandler); //404
//llama error middleware
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

//puerto de escucha
app.listen(config.port, function(){
    console.log('Listening http://localhost:${configs.port}');
});

