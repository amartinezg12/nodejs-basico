const { request } = require('express');
const express = require('express');
//importa clase de servicio
const MoviesService = require('../services/movies');

const {
    movieIdSchema,
    createMovieSchema,
    updateMovieSchema
}=require('../utils/schemas/movies');

const validationHandler = require('../utils/middleware/validationHandler')

const cacheResponse = require('../utils/cache/cacheResponse');
const { FIVE_MINUTES_IN_SECONDS, SIXTY_MINUTES_IN_SECONDS} = require('../utils/cache/time.js');
function moviesApi(app){
    const router = express.Router();
    app.use("/api/movies", router);
    //instancia clase de servicio
    const moviesService = new MoviesService();

    router.get("/", async function(request, response, next){
        //aplica cache
        cacheResponse(response, FIVE_MINUTES_IN_SECONDS);
        const {tags}  = request.query;
       
       try {
            //espera una promesa, responder el objeto mock de momento
            const movies = await moviesService.getMovies({tags});
            //respuesta al navegador
            response.status(200).json({
                data: movies,
                messagge: 'movies listed'        
            });
        } catch (error) {
            next(error);
        }
    });
    //inyecta validaciones
    router.get("/:movieId"  , validationHandler({movieId: movieIdSchema}, 'params'), async function(request, response, next){
        cacheResponse(response, SIXTY_MINUTES_IN_SECONDS);
        let {movieId}  = request.params;
        try {
            //espera una promesa, responde el objeto mock de momento
            const createMovie = await moviesService.getMovie({movieId});
            //respuesta al navegador
            response.status(200).json({
                data: createMovie,
                messagge: 'movie get'        
            });
        } catch (error) {
            next(error);
        }
    });

    router.post("/", validationHandler(createMovieSchema), async function(request, response, next){

        const  {body: movie}   = request;

        try {
            //espera una promesa, respondera el objeto mock de momento
            const movieGetById = await moviesService.createMovie({movie});
            //respuesta al navegador
            response.status(201).json({
                data: movieGetById,
                messagge: 'movie created'        
            });
        } catch (error) {
            next(error);
        }
    });

    router.put("/:movieId", validationHandler({movieId: movieIdSchema}, 'params'), validationHandler(updateMovieSchema),async function(request, response, next){
        //movie es un alias del body
        const  {body: movie}   = request;
        const {movieId}  = request.params;
        try {
            //espera una promesa, respondera el objeto mock de momento
            const movieUpdate = await moviesService.movieUpdate({movie, movieId});
            //respuesta al navegador
            response.status(200).json({
                data: movieUpdate,
                messagge: 'movies updated'        
            });
        } catch (error) {
            next(error);
        }
    });
    router.delete("/:movieId", validationHandler({movieId: movieIdSchema}, 'params'), async function(request, response, next){
        
        const {movieId}   = request.params;

        try {
            //espera una promesa, respondera el objeto mock de momento
            const movieDelete = await moviesService.deleteMovie({movieId});
            //respuesta al navegador
            response.status(200).json({
                data: movieDelete,
                messagge: 'movie deleted'        
            });
        } catch (error) {
            next(error);
        }
    });

    router.patch("/:moviesId", async function(request, response, next){
        //movie es un alias del body
        const  {body: movie}   = request;
        const moviesId   = request.params;
        try {
            //espera una promesa, respondera el objeto mock de momento
            const movieUpdate = await moviesService.patchMovie(movie, moviesId);
            //respuesta al navegador
            response.status(200).json({
                data: movieUpdate,
                messagge: 'movies updated'        
            });
        } catch (error) {
            next(error);
        }
    });


}

    //se exporta la aplicacion para consumirla desde el index de
    module.exports = moviesApi;

