const MongoLib = require('../lib/mongo');
const { moviesMock } = require('../utils/mocks/movies');

class MoviesService{

    //contructor de mongo
    
    constructor(){
        this.collection = 'movies';
        this.mongoDB = new MongoLib;
    }

    async getMovies({tags}){
        const query = tags && { tags: {$in: tags}};//sintaxis de mongo para recibir query por parametro
        const movies = await this.mongoDB.getAll(this.collection , query);
        return movies || [];
    }

    async getMovie({movieId}){
        const movie = await this.mongoDB.get(this.collection , movieId);
        return movie || {};
    }

    async createMovie({movie}){
        const createMoveId = await this.mongoDB.create(this.collection , movie);
        return createMoveId;
    }

    async movieUpdate({movie, movieId} = {}){
        const updateMoveId = await this.mongoDB.update(this.collection , movieId, movie);
        return updateMoveId;
    }

    async deleteMovie({movieId}){
        const deleteMoveId = await this.mongoDB.delete(this.collection , movieId);
        return deleteMoveId;

    }
    //metodos mock 
    /*async getMovies(){
        const movies = await Promise.resolve(moviesMock);
        return movies || [];
    }

    async getMovie(){
        const movie = await Promise.resolve(moviesMock[0]);
        return movie || {};
    }

    async createMovie(){
        const createMoveId = await Promise.resolve(moviesMock[0].id);
        return createMoveId;
    }

    async  movieUpdate(){
        const updateMoveId = await Promise.resolve(moviesMock[0].id);
        return updateMoveId;
    }

    async deleteMovie(){
        const deleteMoveId = await Promise.resolve(moviesMock[0].id);
        return deleteMoveId;

    } */
    async patchMovie(){
        const deleteMoveId = await Promise.resolve(moviesMock[0].id);
        return deleteMoveId;
    }


}


module.exports = MoviesService;