const assert = require('assert');//comparacion en test
const proxyquire = require('proxyquire');//mock

const { moviesMock, MoviesServiceMock } = require('../utils/mocks/movies.js');
const testServer = require('../utils/testServer');
//cambia ruta real por el mock con proxyquire 
describe('routes - movies', function() {
  const route = proxyquire('../routes/movies', {
    '../services/movies': MoviesServiceMock 
  });

  const request = testServer(route);
  describe('GET /movies', function() {
    it('should respond with status 200', function(done) {
      request.get('/api/movies').expect(200, done);//assert
    });
    //lista de peliculas
    it('should respond with the list of movies', function(done) {
      request.get('/api/movies').end((err, res) => {
        assert.deepStrictEqual(res.body, {
          data: moviesMock,
          messagge: 'movies listed'  
        });

        done();
      });
    });
  

  });
  
  describe('GET /movies/:movieId', function () {
    const movieId = moviesMock[0].id;
    it('Should respond with status 200', function (done) {
      request.get('/api/movies/'+ movieId).expect(200, done);
    });

    it('Should respond with the movie retrieve', function (done) {
      request.get('/api/movies/'+ movieId).end((err, res) => {
          assert.deepStrictEqual(res.body, {
            data: moviesMock[0],
            messagge: 'movie get',
          });

          done();
        });
    });
  });


  describe('POST /movies/', function () {
    it('Should respond with status 201', function (done) {
      request.post('/api/movies').expect(201, done);
    });

    it('Should respond with the movie created id', function (done) {
      request.post('/api/movies').send(moviesMock[1]).end((err, res) => {
          assert.deepStrictEqual(res.body, {
            data: moviesMock[0].id,
            messagge: 'movie created',
          });

          done();
        });
    });
  });


});