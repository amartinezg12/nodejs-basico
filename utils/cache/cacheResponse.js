const { config } = require('../../config');

function cacheResponse(res, seconds) {
  if (!config.dev) {//con esto no se aplica cache a desarrollo
    res.set('Cache-Control', `public, max-age=${seconds}`);
  }
}

module.exports = cacheResponse;