const { config } = require('../../config');
const boom = require('@hapi/boom');

function withErrorStack(error, stack) {
  if (config.dev) {//config desarrollo
    // hacer un spread-operator al error porque ahora el error no solo trae el mensaje, si no que trae las propiedades de boom
    return { ...error, stack };
  }

  return error;
}

function logErrors(err, req, res, next) {
  console.log(err);
  next(err);
}

function errorHandler(err, req, res, next) { // eslint-disable-line

const { output: {statusCode, payload}} = err;
  res.status(statusCode|| 500);
  res.json(withErrorStack(payload, err.stack));
}

function wrapErrors(err, req, res, next) {
    // los errores no boom se parsearan a boom
    if (!err.isBoom) {
      next(boom.badImplementation(err));
    }
    // Si el error que estamos pasando por acá es de tipo boom, 
    // llamamos al siguiente middleware con el error
    next(err);
  }

module.exports = {
  logErrors,
  errorHandler,
  wrapErrors
};