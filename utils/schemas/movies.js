const joi = require('@hapi/joi');
//crear schema de validacion de pelicula para e joi del validationHandler
//uno para create y otro para update

const movieIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/); //valida que sea string y cumpla con la regex
const movieTitleSchema = joi.string().max(80); //string de 80 maximo
const movieYearSchema = joi
  .number()
  .min(1888)
  .max(2077);
const movieCoverSchema = joi.string().uri(); //solo string con url
const movieDescriptionSchema = joi.string().max(300);
const movieDurationSchema = joi
  .number()
  .min(1)
  .max(300);
const movieContentRatingSchema = joi.string().max(5);
const movieSourceSchema = joi.string().uri();
const movieTagsSchema = joi.array().items(joi.string().max(50));
//llama atributos para la creacion
const createMovieSchema = {
  title: movieTitleSchema.required(), //requerido
  year: movieYearSchema.required(),
  cover: movieCoverSchema.required(),
  description: movieDescriptionSchema.required(),
  duration: movieDurationSchema.required(),
  contentRating: movieContentRatingSchema.required(),
  source: movieSourceSchema.required(),
  tags: movieTagsSchema
};
//para la actualizacion
const updateMovieSchema = {
  title: movieTitleSchema,
  year: movieYearSchema,
  cover: movieCoverSchema,
  description: movieDescriptionSchema,
  duration: movieDurationSchema,
  contentRating: movieContentRatingSchema,
  source: movieSourceSchema,
  tags: movieTagsSchema
};

module.exports = {
  movieIdSchema,
  createMovieSchema,
  updateMovieSchema
};